# StreamBlasters

This is web crawler and a search engine written to crawl and index most of the pirated sites for future use.

# Current URLs
- https://www.streamblasters.fans/

# TODO
- [ ] https://tamilblasters.fans/
- [ ] https://movie-web.app/
- [ ] https://braflix.app/

## Note: This is not a streaming service nor a movie downloader. This is just a search engine for websites that use streaming services.

[![Get it on Codeberg](undefined.png)](https://codeberg.org/yashasnadigsyn/streamblasters.rs/)

