use std::collections::HashMap;
use std::collections::VecDeque;

fn main() {

    let url: String = String::from("https://www.streamblasters.fans/");
    let mut all_urls = Vec::new();
    let mut all_bfs_urls: VecDeque<String> = VecDeque::new();
    let mut all_final_urls: Vec<String>= Vec::new();
    let mut counter = 0;
    all_bfs_urls.push_back(url.clone());
    println!("Started!");
    loop {
        counter += 1;
        if all_bfs_urls.len() == 0 {
            break;
        }
        if counter%20 == 0 {
            break
        }
        let current_url = all_bfs_urls.get(0).unwrap();
        let mut hashy = HashMap::new();
        println!("{}", &current_url);
        let resp = reqwest::blocking::get(&url);
        let cont = resp.unwrap().text().unwrap();
        //println!("{}", &cont);
        let doc = scraper::Html::parse_document(&cont);
        let whole_cont_selector = scraper::Selector::parse("body").unwrap();
        let mut whole_cont = doc.select(&whole_cont_selector).flat_map(|x| x.text()).collect::<String>();
        let a_links_selector = scraper::Selector::parse("a").unwrap();
        let a_links = doc.select(&a_links_selector);
        whole_cont = whole_cont.replace("\t", " ").replace("\n", " ");
        println!("{}", whole_cont);
        hashy.insert(current_url.clone(), whole_cont.clone());
        all_urls.push(hashy.clone());
        //println!("{}", cont);

        for a_link in a_links {
            if a_link.attr("href").unwrap().starts_with("https://www.streamblasters.fans/") &&  !all_final_urls.contains(&(a_link.attr("href").unwrap()).to_string()) {
                all_bfs_urls.push_back(a_link.attr("href").unwrap().to_string());
                all_final_urls.push(a_link.attr("href").unwrap().to_string());
            }
        }
        all_bfs_urls.pop_front();
    }
    let json = serde_json::to_string(&all_urls).unwrap();
    std::fs::write("urls.json", json).expect("ERROR!");
    //std::fs::write("urls.json", all_urls).expect("ERROR!");
}
